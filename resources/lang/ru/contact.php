<?php
return [
    "title"   => "КОНТАКТЫ",
    "address" => "Саят-Нова 40, 0001, Ереван, РА",
    "phone_title" => "Номер",
    "copyright" => "2021 Фонд «Ваге Меликсетян»",
    "meta_title"  => "КОНТАКТЫ | VaheMeliksetyan Fund",
    "meta_description" => "Meta Description"
];

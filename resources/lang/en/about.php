<?php
return [
    "title" => "ABOUT US",
    "description" => "\"VAHE MELIKSETYAN\" Educational-Medical Foundation (hereinafter referred to as \"the Foundation\") is a non-commercial non-profit organization created by the founder on the basis of voluntary property payments, which pursues educational, social, cultural, charitable and other beneficial purposes. The Foundation was established by the decision of the founder, January 8, 2021, No. 1; The activities of the Foundation are based on the principles of legality, publicity, self-government, equality and volunteering.",
    "meta_title"  => "ABOUT US | VaheMeliksetyan Fund",
    "meta_description" => "Meta Description",
];

<?php

return [
    "title" => "NEWS",
    "title_2" => "Decision No. 1 of the founder on establishing \"VAHE MELIKSETYAN\" educational-medical foundation",
    "address" => "Yerevan, Armenia",
    "date" => "\"08\" January 2021",
    "sub_title" => "Sargis Samvel Meliksetyan, the founder of \"VAHE MELIKSETYAN\" Educational-Medical Foundation.",
    "decide" => "DECIDED",
    "text_1" => "To establish \"VAHE MELIKSETYAN\" Educational-Medical Foundation (hereinafter referred to as the Foundation),",
    "text_2" => "To approve the charter of the Foundation,",
    "text_3" => "Appoint Sargis Samvel Meliksetyan as Acting Executive Director of the Foundation, authorize him to perform all the work related to the state registration of the Foundation.",
    "meta_title"  => "NEWS | VaheMeliksetyan Fund",
    "meta_description" => "Meta Description",
];

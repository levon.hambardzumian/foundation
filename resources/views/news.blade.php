@extends('layouts.app')

@section('title')
    <title>{!! trans('news.meta_title') !!}</title>
    <meta property="og:title" content="{!! trans('news.meta_title') !!}">
@endsection

@section('content')
    <div class="news-container">
        <div class="news-box">
            <h1 class="title">{!! trans('news.title') !!}</h1>
            <div class="news-container-box">
                <div class="news-logo"></div>
                <div class="news-description">
                    <div class="news-description-text">
                        <p>{!! trans('news.title_2') !!}</p>
                    </div>
                    <div class="news-description-text-3">
                        <div>{!! trans('news.address') !!}</div>
                        <div class="news date">{!! trans('news.date') !!}</div>
                    </div>
                    <div class="news-description-text-2">
                        <p>{!! trans('news.sub_title') !!}</p>
                    </div>
                    <div class="news-description-text-4">
                        <p>{!! trans('news.decide') !!}</p>
                    </div>
                    <div class="news-description-text-5">
                       <ul>
                           <li>{!! trans('news.text_1') !!}</li>
                           <li>{!! trans('news.text_2') !!}</li>
                           <li>{!! trans('news.text_3') !!}</li>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('title')
    <title>{!! trans('about.meta_title') !!}</title>
    <meta property="og:title" content="{!! trans('about.meta_title') !!}">
@endsection

@section('content')
    <div class="about-container">
        <div class="about-description-box">
            <h1 class="title">{!! trans('about.title') !!}</h1>
            <div class="about-description">
                {!! trans('about.description') !!}
            </div>
            <div class="person-image"></div>
        </div>
    </div>
@endsection
